FROM docker:23.0.5

ENV PYTHONUNBUFFERED=1

ENV ANSIBLE_VERSION=7.5.0
ENV MOLECULE_VERSION=5.0.1
ENV MOLECULE_DOCKER_VERSION=2.1.0

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python && \
    python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools

RUN pip3 install \
    ansible==${ANSIBLE_VERSION} \
    molecule==${MOLECULE_VERSION} \
    molecule-docker==${MOLECULE_DOCKER_VERSION}

